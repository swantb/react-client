# react-client

## 简介
通过create-react-app创建的typescript前端应用demo

## 说明
1. 终端执行命令: npm run init  下载依赖包
2. 终端执行命令：npm run build  打包
3. 终端执行命令: npm run start  启动程序

## 常见问题与解决
* 问题：windows下build可能会报错" Delete '␍'  prettier/prettier"
* 解决：修改git全局配置`git config --global core.autocrlf false`;然后重新拉取代码

* 问题：怎么修改启动端口
* 解决：修改craco.config.js文件中的`process.env.PORT`变量
