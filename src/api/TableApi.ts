import { getRequest } from "../utils/fetch";

class TableApi {
  urlPrefix = "sys";

  public async searchTableOptions(keywords: string) {
    const res = await getRequest(
      { keywords },
      { urlPrefix: `${this.urlPrefix}/tableOptions` }
    );
    return res;
  }

  public async getTableFields(tableName: string, pagination) {
    const res = await getRequest(
      { tableName },
      { urlPrefix: `${this.urlPrefix}/tableFields`, pagination }
    );
    return res;
  }
}

export default new TableApi();
