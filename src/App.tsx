import { Routes, Route } from "react-router-dom";
import Main from "./components/Main";
import { RouteConfig } from "./routes";
import { withRouter } from "./utils/functions";

function makeRoutes(routes: RouteConfig[]): React.ReactElement[] {
  if (!routes) {
    return null;
  }
  return routes.map((route) => {
    const element = route.component
      ? withRouter(route.component)({ routes: route.routes })
      : undefined;
    return (
      <Route key={route.name} path={route.path} element={element}>
        {makeRoutes(route.routes)}
      </Route>
    );
  });
}

function App({ title, desc, routes, ...porps }) {
  const subRoutes = makeRoutes(routes);
  return (
    <Routes>
      <Route element={<Main title={title} desc={desc} routes={routes} />}>
        {subRoutes}
      </Route>
    </Routes>
  );
}

export default App;
