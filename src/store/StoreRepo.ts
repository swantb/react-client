import DemoStore from "./stores/DemoStore";
import LoginStore from "./stores/LoginStore";
import RouterStore from "./stores/RouterStore";

export default class StoreRepo {
  loginStore: LoginStore;
  demoStore: DemoStore;
  routerStore: RouterStore;
  constructor() {
    this.loginStore = new LoginStore();
    this.demoStore = new DemoStore();
    this.routerStore = new RouterStore();
  }
}
