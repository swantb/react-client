import React from "react";
import { useContext } from "react";
import StoreRepo from "./StoreRepo";

const StoreContext = React.createContext(new StoreRepo());
const useStores = () => useContext(StoreContext);

export { useStores };
