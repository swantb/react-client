import { makeObservable } from "mobx";
import BaseStore from "../BaseStore";

class RouterStore extends BaseStore {
  urlPrefix = "sys/router";
  constructor() {
    super();
    makeObservable(this);
  }
}

export default RouterStore;
