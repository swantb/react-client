import { makeObservable } from "mobx";
import BaseStore from "../BaseStore";

class DemoStore extends BaseStore {
  urlPrefix = "app/demo";
  constructor() {
    super();
    makeObservable(this);
  }
}

export default DemoStore;
