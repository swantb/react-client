import { action, makeObservable, observable, runInAction } from "mobx";
import { getRequest, postRequest } from "../../utils/fetch";

class LoginStore {
  @observable isLogin = false;
  @observable firstLogin = true;
  @observable currentUser = null;
  @action handleLocalLogin = async (values: any) => {
    if (!values.account || !values.password) {
      return;
    }
    const userInfo = await postRequest(values, { urlPrefix: "app/login" });
    if (userInfo) {
      runInAction(() => {
        this.isLogin = true;
        this.currentUser = userInfo;
      });
    }
    return userInfo;
  };
  @action handleLogout = async () => {
    const res = await getRequest(null, { urlPrefix: "app/logout" });
    if (res) {
      runInAction(() => {
        this.isLogin = false;
        this.firstLogin = true;
        this.currentUser = null;
      });
    }
  };
  @action set = (key: string, value: any) => {
    this[key] = value;
  };
  constructor() {
    makeObservable(this);
  }
}

export default LoginStore;
