import _ from "lodash";
import {
  action,
  configure,
  makeObservable,
  observable,
  runInAction,
} from "mobx";
import {
  deleteRequest,
  getRequest,
  postRequest,
  putRequest,
} from "../utils/fetch";

configure({ enforceActions: "always" });

class BaseStore {
  urlPrefix = "";
  @observable fetchLoading = false;
  @observable dataSource: Array<any> = [];
  @observable selectedItem: any = {};
  @observable queryParam: any = {};
  @observable pagination = {
    current: 1,
    pageSize: 10,
  };
  @action fetchDataSource = async (
    queryParam = this.queryParam,
    {
      urlPrefix = this.urlPrefix,
      urlSuffix = "",
      pagination = this.pagination,
      options = {},
    } = {}
  ) => {
    runInAction(() => {
      this.fetchLoading = true;
    });
    const res = await getRequest(queryParam, {
      urlPrefix: `${urlPrefix}/${urlSuffix}`,
      pagination,
      options,
    });
    runInAction(() => {
      this.fetchLoading = false;
      this.queryParam = queryParam;
      this.pagination = pagination;
      if (res?.success) {
        this.dataSource = res.data || [];
        this.pagination = res.pagination;
      }
    });
    return res;
  };
  @action fetchItemById = async (
    id: string,
    { urlPrefix = this.urlPrefix } = {}
  ) => {
    const res = await getRequest({}, { urlPrefix: `${urlPrefix}/${id}` });
    runInAction(() => {
      if (res) {
        this.selectedItem = res || {};
      }
    });
    return res;
  };
  @action createItem = async (
    data: Record<string, any>,
    { autoFetchList = true } = {}
  ) => {
    const res = await postRequest(data, { urlPrefix: this.urlPrefix });
    if (!_.isEmpty(res) && autoFetchList) {
      this.fetchDataSource(this.queryParam);
    }
    return res;
  };
  @action updateItemById = async (
    data: Record<string, any>,
    id: string,
    { autoFetchList = true } = {}
  ) => {
    const res = await putRequest(
      data,
      {},
      { urlPrefix: `${this.urlPrefix}/${id}` }
    );
    if (!_.isEmpty(res) && autoFetchList) {
      this.fetchDataSource(this.queryParam);
    }
    return res;
  };
  @action deleteItemById = async (
    id: string,
    { autoFetchList = true } = {}
  ) => {
    const res = await deleteRequest(`${this.urlPrefix}/${id}`);
    if (res && autoFetchList) {
      this.fetchDataSource(this.queryParam);
    }
    return res;
  };
  @action set = (key: string, value: any) => {
    this[key] = value;
  };
  @action setQueryParam = (val: any) => {
    this.queryParam = { ...val };
  };
  @action setPagination = (val: any) => {
    this.pagination = { ...val };
  };
  @action resetAll = () => {
    this.fetchLoading = false;
    this.dataSource = [];
    this.selectedItem = {};
    this.queryParam = {};
    this.pagination = {
      current: 1,
      pageSize: 10,
    };
  };
  constructor() {
    makeObservable(this);
  }
}

export default BaseStore;
