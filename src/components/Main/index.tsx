import { Outlet } from "react-router-dom";
import Head from "./Head";

function Main({ title, desc, routes, ...props }) {
  document.title = title;

  return (
    <div>
      <Head title={title} desc={desc} routes={routes} />
      <Outlet />
    </div>
  );
}

export default Main;
