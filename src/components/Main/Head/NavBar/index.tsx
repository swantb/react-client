import React from "react";
import { NavLink } from "react-router-dom";
import "./index.css";
function NavBar({ routes, ...props }) {
  return (
    <div className="navbar">
      {routes.map((route) => {
        const style: React.CSSProperties = {
          float: route.onRight ? "right" : "left",
        };
        return (
          <NavLink to={route.path} className="navlink" key={route.name}>
            <div className="col-2" style={style}>
              {route.name}
            </div>
          </NavLink>
        );
      })}
    </div>
  );
}

export default NavBar;
