import styles from "./index.module.less";
import logo from "../../../resources/images/ico192.png";
import NavBar from "./NavBar";

function Head({ title, desc, routes, ...props }) {
  document.title = title;
  return (
    <div>
      <div className={styles.header}>
        <img className={styles.logo} src={logo} alt="logo" />
        <span className={styles.title}>{title}</span>
        <div className={styles.desc}>{desc || " "}</div>
        <NavBar routes={routes}></NavBar>
      </div>
    </div>
  );
}

export default Head;
