import { message } from "antd";
import fetch from "axios";
import _ from "lodash";

const _handleResponseError = (res: any) => {
  let errMsg = "";
  let resp = res;
  if (resp.response) {
    resp = resp.response;
  }
  const statusCode = res.status;
  if (statusCode === 200) {
    errMsg = res.data.errorMsg || res.data.message;
  } else if (statusCode === 404) {
    errMsg = "404 not found";
  } else {
    errMsg = `系统异常(${statusCode})`;
  }
  if (!errMsg) {
    errMsg = "未知错误";
  }
  message.error(errMsg);
};

const getRequest = async (
  query: Record<string, any>,
  { urlPrefix = "", pagination = {}, options = {} } = {}
) => {
  let res: any = {};
  let params = {};
  if (!_.isNil(query)) {
    params = { query };
  }
  if (!_.isEmpty(pagination)) {
    params = { ...params, pagination };
  }
  if (!_.isEmpty(options)) {
    params = { ...params, options };
  }
  try {
    res = await fetch({
      method: "GET",
      url: `/${urlPrefix}`,
      params,
    });
  } catch (e) {
    res = e;
  }
  if (res.status !== 200 || !res.data.success) {
    return _handleResponseError(res);
  }
  return res.data;
};

const putRequest = async (
  data: Record<string, any>,
  query: Record<string, any>,
  { urlPrefix = "" }
) => {
  let res: any = {};
  try {
    res = await fetch({
      method: "PUT",
      url: `/${urlPrefix}`,
      data: {
        data,
        query,
      },
    });
  } catch (e) {
    res = e;
  }
  if (res.status !== 200 || !res.data.success) {
    _handleResponseError(res);
    return null;
  }

  return res.data.data;
};

const postRequest = async (
  data: Record<string, any>,
  { urlPrefix = "", headers = {} } = {}
) => {
  let res: any = {};
  try {
    res = await fetch({
      method: "POST",
      url: `/${urlPrefix}`,
      data,
      headers,
    });
  } catch (e) {
    res = e;
  }
  if (res.status !== 200 || !res.data.success) {
    return _handleResponseError(res);
  }
  return res.data.data;
};

const deleteRequest = async (urlPrefix = "") => {
  let res: any = {};
  try {
    res = await fetch({
      method: "DELETE",
      url: `/${urlPrefix}`,
    });
  } catch (e) {
    res = e;
  }
  if (res.status !== 200 || !res.data.success) {
    return _handleResponseError(res);
  }
  return res.data.data;
};

export { getRequest, putRequest, postRequest, deleteRequest };
