import { useLocation, useNavigate, useParams } from "react-router-dom";

export function getSearchQuery(key: string) {
  const searchUrl = window.location.search;
  if (!searchUrl) {
    return "";
  }
  if (searchUrl.indexOf(key) >= 0) {
    const searchSplits = searchUrl.split("&");
    const urlEncoded = searchSplits.filter((s) => s.indexOf(key) >= 0)[0];
    if (urlEncoded) {
      return decodeURIComponent(urlEncoded.split("=")[1]);
    }
  }
  return "";
}

export function withRouter(Component) {
  function ComponentWithRouterProp(props) {
    const location = useLocation();
    const navigate = useNavigate();
    const params = useParams();
    return <Component {...props} router={{ location, navigate, params }} />;
  }
  return ComponentWithRouterProp;
}
