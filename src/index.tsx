import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import routes from "./routes";
import zh_CN from "antd/lib/locale/zh_CN";
import { BrowserRouter } from "react-router-dom";
import { ConfigProvider } from "antd";

ReactDOM.render(
  <ConfigProvider locale={zh_CN}>
    <BrowserRouter>
      <App title="React Client" desc="测试阶段" routes={routes} />
    </BrowserRouter>
  </ConfigProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
