import {
  BranchesOutlined,
  BulbOutlined,
  SettingOutlined,
  TableOutlined,
} from "@ant-design/icons";
import AboutPage from "./pages/AboutPage";
import ClientPage from "./pages/ClientPage";
import LoginPage from "./pages/ClientPage/LoginPage";
import ClientLayout from "./pages/ClientPage/MainPage";
import DemoPage from "./pages/ClientPage/MainPage/Demo";
import RouterPage from "./pages/ClientPage/MainPage/System/Router";
import TablePage from "./pages/ClientPage/MainPage/System/Table";
import HomePage from "./pages/HomePage";

export type RouteConfig = {
  name: string;
  path: string;
  component?: (props) => JSX.Element;
  icon?: React.ReactNode;
  routes?: RouteConfig[];
  [key: string]: any;
};

const routes: RouteConfig[] = [
  {
    name: "主页",
    path: "/",
    component: HomePage,
  },
  {
    name: "后端接口测试",
    path: "client",
    component: ClientPage,
    routes: [
      {
        name: "login",
        path: "login",
        component: LoginPage,
      },
      {
        name: "client-main",
        path: "",
        component: ClientLayout,
        routes: [
          {
            name: "开发模版",
            path: "demo",
            icon: <BulbOutlined />,
            component: DemoPage,
          },
          {
            name: "后台管理",
            path: "backstage",
            icon: <SettingOutlined />,
            routes: [
              {
                name: "路由列表",
                path: "router",
                icon: <BranchesOutlined />,
                component: RouterPage,
              },
              {
                name: "数据库表定义",
                path: "table",
                icon: <TableOutlined />,
                component: TablePage,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    name: "关于",
    path: "about",
    onRight: true,
    component: AboutPage,
  },
];

export default routes;
