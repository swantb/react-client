import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { LoginForm, ProFormText } from "@ant-design/pro-form";
import { message } from "antd";
import { observer } from "mobx-react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useStores } from "../../../store";

function LoginPage(props) {
  const store = useStores();
  const { loginStore } = store;
  const { isLogin } = loginStore;
  const navigate = useNavigate();
  const handleSubmit = async (values: any) => {
    const loginRes = await loginStore.handleLocalLogin(values);
    console.log(loginRes);
  };
  const handleSubmitFailed = async (errorInfo: any) => {
    message.error("登入失败");
  };
  useEffect(() => {
    if (isLogin) {
      message.info("登入成功");
      loginStore.set("firstLogin", false);
      navigate("..");
    }
  }, [isLogin]);
  return (
    <LoginForm onFinish={handleSubmit} onFinishFailed={handleSubmitFailed}>
      <ProFormText
        label="账号"
        name="account"
        fieldProps={{
          size: "large",
          prefix: <UserOutlined className={"prefixIcon"} />,
        }}
        placeholder={"请输入账号"}
        rules={[
          {
            required: true,
            message: "请输入账号!",
          },
        ]}
      />
      <ProFormText.Password
        label="密码"
        name="password"
        fieldProps={{
          size: "large",
          prefix: <LockOutlined className={"prefixIcon"} />,
        }}
        placeholder={"请输入密码"}
        rules={[
          {
            required: true,
            message: "请输入密码!",
          },
        ]}
      />
    </LoginForm>
  );
}

export default observer(LoginPage);
