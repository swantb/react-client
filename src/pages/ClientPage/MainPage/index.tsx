import { useEffect, useState } from "react";
import { Link, Outlet, useLocation, useNavigate } from "react-router-dom";
import { useStores } from "../../../store";
import ProLayout from "@ant-design/pro-layout";
import _ from "lodash";

const ClientLayout = ({ routes, ...props }) => {
  const store = useStores();
  const { loginStore } = store;
  const { isLogin } = loginStore;
  const navigate = useNavigate();
  const location = useLocation();
  const [route, setRoute] = useState({});
  function getFullPath(srcRoutes, parentPath, menuLevel) {
    if (!srcRoutes) {
      return undefined;
    }
    return srcRoutes.map((route) => {
      const path = _.replace(`${parentPath}/${route.path}`, /\/+/g, "/");
      const routes = getFullPath(route.routes, path, menuLevel + 1);
      return {
        ...route,
        path,
        routes,
        level: menuLevel,
      };
    });
  }
  useEffect(() => {
    if (!isLogin) {
      navigate("login");
    }
    const parentPath = location.pathname;
    setRoute({ path: parentPath, routes: getFullPath(routes, parentPath, 1) });
  }, []);
  return (
    <ProLayout
      title="Client"
      layout="side"
      navTheme="light"
      location={location}
      route={route}
      onMenuHeaderClick={(e) => {
        navigate("");
      }}
      breadcrumbProps={{
        itemRender: (route, params, routes, paths) => {
          return <span>{route.breadcrumbName}</span>;
        },
      }}
      menuItemRender={(item, dom, menuProps) => {
        if (item.isUrl || !item.path) {
          return dom;
        }
        const subMenuIcon = item.level > 1 ? item.icon : undefined;
        return (
          <Link to={item.path}>
            {subMenuIcon}
            {dom}
          </Link>
        );
      }}
      {...props}
    >
      <Outlet />
    </ProLayout>
  );
};

export default ClientLayout;
