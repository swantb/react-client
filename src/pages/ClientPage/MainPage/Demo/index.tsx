import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { ModalForm, ProFormText } from "@ant-design/pro-form";
import { PageContainer } from "@ant-design/pro-layout";
import ProTable, { ProColumns } from "@ant-design/pro-table";
import { Button, message, Popconfirm, Popover, Space } from "antd";
import { observer } from "mobx-react";
import { useEffect, useState } from "react";
import { useStores } from "../../../../store";
import { DANGER_COLOR } from "../../../../utils/colors";

function DemoPage(porps) {
  const store = useStores();
  const { demoStore } = store;
  const { fetchLoading, dataSource, pagination } = demoStore;
  const [modalVisible, setModalVisible] = useState(false);
  useEffect(() => {
    demoStore.fetchDataSource();
    return demoStore.resetAll;
  }, []);
  const handleQuery = async (params) => {
    const res = await demoStore.fetchDataSource(params);
    return !!res;
  };
  const handleFinish = async (values) => {
    const res = await demoStore.createItem(values);
    if (res) {
      message.success("创建成功");
      return res;
    }
    message.error("创建失败");
  };
  const onDelete = async (record) => {
    const res = demoStore.deleteItemById(record.oid);
    if (res) {
      message.success("删除成功");
    }
  };

  const columns: ProColumns[] = [
    {
      title: "名称",
      dataIndex: "name",
    },
    {
      title: "编码",
      dataIndex: "code",
    },
    {
      title: "操作",
      valueType: "option",
      render: (dom, record, index, action) => (
        <Space size="middle">
          <Popconfirm
            title="确定删除此条目？"
            onConfirm={() => onDelete(record)}
          >
            <Popover content="删除">
              <DeleteOutlined style={{ color: DANGER_COLOR }} />
            </Popover>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <PageContainer>
      <ProTable
        rowKey="oid"
        columnEmptyText="-"
        columns={columns}
        loading={fetchLoading}
        dataSource={dataSource}
        pagination={pagination}
        options={{ fullScreen: false, reload: false, setting: true }}
        dateFormatter="string"
        onSubmit={handleQuery}
        toolBarRender={() => [
          <Button
            key="button"
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => {
              setModalVisible(true);
            }}
          >
            新建
          </Button>,
        ]}
      />
      <ModalForm
        title="新增Demo"
        isKeyPressSubmit
        modalProps={{ maskClosable: true, destroyOnClose: true }}
        visible={modalVisible}
        onVisibleChange={(visiable) => {
          setModalVisible(visiable);
        }}
        onFinish={handleFinish}
      >
        <ProFormText
          label="编码"
          name="code"
          placeholder="请输入编码"
          rules={[
            {
              required: true,
              message: "请输入编码!",
            },
          ]}
        />
        <ProFormText
          label="名称"
          name="name"
          placeholder="请输入名称"
          rules={[
            {
              required: true,
              message: "请输入名称!",
            },
          ]}
        />
      </ModalForm>
    </PageContainer>
  );
}

export default observer(DemoPage);
