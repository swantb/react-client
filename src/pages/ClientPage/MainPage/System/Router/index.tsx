import { PageContainer } from "@ant-design/pro-layout";
import ProTable, { ProColumns } from "@ant-design/pro-table";
import { observer } from "mobx-react";
import { useEffect } from "react";
import { useStores } from "../../../../../store";

function RouterPage(props) {
  const store = useStores();
  const { routerStore } = store;
  const { fetchLoading, dataSource, pagination } = routerStore;
  useEffect(() => {
    routerStore.fetchDataSource();
    return routerStore.resetAll;
  }, []);
  const columns: ProColumns[] = [
    {
      title: "名称",
      dataIndex: "name",
    },
    {
      title: "路由",
      dataIndex: "path",
    },
    {
      title: "请求方式",
      dataIndex: "method",
    },
    {
      title: "说明",
      dataIndex: "desc",
      search: false,
    },
  ];
  const handleQuery = async (params) => {
    const res = await routerStore.fetchDataSource(params);
    return !!res;
  };
  return (
    <PageContainer>
      <ProTable
        rowKey="name"
        columnEmptyText="-"
        columns={columns}
        loading={fetchLoading}
        dataSource={dataSource}
        pagination={pagination}
        options={{ fullScreen: false, reload: false, setting: true }}
        onSubmit={handleQuery}
      />
    </PageContainer>
  );
}

export default observer(RouterPage);
