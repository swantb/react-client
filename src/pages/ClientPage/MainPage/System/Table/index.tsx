import { ProFormSelect } from "@ant-design/pro-form";
import { PageContainer } from "@ant-design/pro-layout";
import ProTable, { ProColumns } from "@ant-design/pro-table";
import { useEffect, useState } from "react";
import TableApi from "../../../../../api/TableApi";

function TablePage(props) {
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 10,
    total: 0,
  });
  const [shouldSearch, setShoudSearch] = useState(false);
  const [tableName, setTableName] = useState("");
  useEffect(() => {
    if (shouldSearch) {
      handleTableQuery(tableName);
      setShoudSearch(false);
    }
  }, [shouldSearch]);
  const handleSearchQuery = async (params) => {
    const res = await TableApi.searchTableOptions(params.tableName);
    return res.data;
  };
  const handleTableQuery = async (params) => {
    setLoading(true);
    const res = await TableApi.getTableFields(params, pagination);
    if (res && res.success) {
      setDataSource(res.data);
      setPagination(res.pagination);
    }
    setLoading(false);
    return !!res;
  };
  const columns: ProColumns[] = [
    {
      title: "表名",
      dataIndex: "tableName",
      valueType: "select",
      hideInTable: true,
      renderFormItem: (_, fieldConfig, form) => {
        return (
          <ProFormSelect
            placeholder="请输入表名查询"
            request={handleSearchQuery}
            showSearch
          />
        );
      },
    },
    {
      title: "字段名称",
      dataIndex: "field",
      search: false,
    },
    {
      title: "字段类型",
      dataIndex: "type",
      search: false,
    },
    {
      title: "默认值",
      dataIndex: "defaultValue",
      search: false,
    },
    {
      title: "是否主键字段",
      dataIndex: "primaryKey",
      renderText: (text: any, record: any) => (record.primaryKey ? "是" : "否"),
      search: false,
    },
    {
      title: "是否唯一键",
      dataIndex: "unique",
      search: false,
      renderText: (text: any, record: any) => (record.unique ? "是" : "否"),
    },
    {
      title: "是否允许Null值",
      dataIndex: "allowNull",
      renderText: (text: any, record: any) => (record.allowNull ? "是" : "否"),
      search: false,
    },
  ];
  return (
    <PageContainer>
      <ProTable
        rowKey="field"
        columnEmptyText="-"
        columns={columns}
        loading={loading}
        dataSource={dataSource}
        search={{ span: 10 }}
        pagination={{
          ...pagination,
          onChange: (page: number, pageSize: number) => {
            setPagination({ current: page, pageSize, total: pagination.total });
            if (page !== pagination.current) setShoudSearch(true);
          },
          onShowSizeChange: (current: number, size: number) => {
            setPagination({ current, pageSize: size, total: pagination.total });
            if (size !== pagination.pageSize) setShoudSearch(true);
          },
        }}
        options={{ fullScreen: false, reload: false, setting: true }}
        onSubmit={(params: any) => {
          setTableName(params.tableName);
          setShoudSearch(true);
        }}
      />
    </PageContainer>
  );
}

export default TablePage;
