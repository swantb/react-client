const AboutPage = () => {
  return (
    <div>
      <p>作者: yilin.li</p>
      <p>
        项目地址
        <a
          target="_blank"
          rel="noreferrer noopener"
          href="https://gitee.com/liyilin666/react-client"
        >
          https://gitee.com/liyilin666/react-client
        </a>
      </p>
    </div>
  );
};

export default AboutPage;
