/* eslint-disable @typescript-eslint/no-var-requires */
const CracoLessPlugin = require("craco-less");

process.env.PORT = 5000;
process.env.GENERATE_SOURCEMAP = "false";
module.exports = {
  babel: {
    plugins: [
      ["@babel/plugin-proposal-decorators", { legacy: true }], //支持装饰器
      [
        "import",
        {
          libraryName: "antd",
          libraryDirectory: "es",
          style: true, //设置为true即是less
        },
      ],
    ],
  },
  //配置代理
  devServer: {
    proxy: {
      "/app": {
        target: "http://localhost:8888/app",
        changeOrigin: true,
        pathRewrite: {
          "^/app": "",
        },
      },
      "/sys": {
        target: "http://localhost:8888/sys",
        changeOrigin: true,
        pathRewrite: {
          "^/sys": "",
        },
      },
    },
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { "@primary-color": "#1DA57A" },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
